## Description

  This task was built on the basis of a [react-boilerplate](https://github.com/react-boilerplate/react-boilerplate/)
  It includes the following stack of technologies:
  
-  React - JavaScript library for building user interfaces. 
-  React Router - declarative routing for React.
-  Redux - predictable state container for JavaScript apps.
-  Redux Saga - side effect model for Redux apps.
-  Reselect - selector library for Redux.
-  ImmutableJS - immutable collections for JavaScript.
-  Styled Components - allows to write actual CSS code to style components.
-  Helmet - library providing react component for managing all changes to document head
-  React-intl - industry-standard i18n internationalization

This is production-ready framework and contains tools for analyze and profile app's performance, AppVeyor and TravisCI setups included by default,   
It has the best developer experience and best practices, allows create from the CLI components, containers, routes, selectors and sagas and their tests.

Also in this task were used material-ui - react components that implement Google's Material Design.

 
###TODO: 
 -  Writing of tests.
 -  Use flow type check
 -  Memoization incoming data
 -  Refactoring of Redux-based codebases to make of reducers and action creators simpler and more declarative (e.g. creation of reducers and action creators, avoid increasing files with constants etc.). 
