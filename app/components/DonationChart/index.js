/**
 *
 * DonationChart
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ComposedChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, LabelList } from 'recharts';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import _isEmpty from 'lodash/isEmpty';

import styles from './styles';

/* eslint-disable react/prefer-stateless-function */
const DonationChart = ({ donations }) => (
  <div>
    <ComposedChart
      width={740}
      height={400}
      data={donations}
      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
      padding={{ left: 30, right: 30 }}
    >
      <XAxis dataKey="created_at" />
      <YAxis label={{ value: 'amount, $', angle: -90, position: 'left' }} />
      <CartesianGrid strokeDasharray="3 3" />

      <Tooltip formatter={value => `${value} $`} />
      <Legend payload={[{ value: _isEmpty(donations) ? 'No data' : 'Date', type: 'line', id: 'ID01' }]} />

      <Line type="monotone" dataKey="amount" stroke="#8884d8" activeDot={{ r: 8 }}>
        <LabelList dataKey="name" position="top" />
      </Line>
    </ComposedChart>
  </div>
);

DonationChart.propTypes = {
  donations: PropTypes.array,
};

export default withStyles(styles)(injectIntl(DonationChart));
