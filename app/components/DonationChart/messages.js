/*
 * DonationChart Messages
 *
 * This contains all the text for the DonationChart component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DonationChart.header',
    defaultMessage: 'This is the DonationChart',
  },
  date_from: {
    id: 'app.components.DonationChart.date_from',
    defaultMessage: 'Date from',
  },
  date_to: {
    id: 'app.components.DonationChart.date_to',
    defaultMessage: 'Date to',
  },
});
