/*
 * DonationsLiderboard Messages
 *
 * This contains all the text for the DonationsLiderboard component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  recurring: {
    id: 'app.components.DonationsLiderboard.recurring',
    defaultMessage: 'Recurring donation',
  },
  one_time: {
    id: 'app.components.DonationsLiderboard.one_time',
    defaultMessage: 'One-time donation',
  },
  regular: {
    id: 'app.components.DonationsLiderboard.regular',
    defaultMessage: 'Regular donor',
  },
  user: {
    id: 'app.components.DonationsLiderboard.regular',
    defaultMessage: 'User',
  },
});
