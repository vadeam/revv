export default theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    paddingBottom: 20,
  },
  icon: {
    fontSize: 20,
    marginRight: 30,
  },
  iconComment: {
    fontSize: 20,
  },
  boardWrapper: {
    overflowX: 'auto',
  },
  EmailText: {
    textAlign: 'left',
    width: '25%',
  },
  DonationTypeText: {
    textAlign: 'left',
  },
  ChipWrapper: {
    display: 'flex',
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  comment: {
    marginRight: 20,
  },
});
