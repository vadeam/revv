/**
 *
 * DonationsLiderboard
 *
 */

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import _isEmpty from 'lodash/isEmpty';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import ImageIcon from '@material-ui/icons/Image';
import PlusOneIcon from '@material-ui/icons/PlusOne';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Filter9PlusIcon from '@material-ui/icons/Filter9Plus';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';

import Comment from 'components/Comment';
import H3 from 'components/H3';
import { DONATION_TYPE_RECURRING, DONATION_TYPE_ONE_TIME, DONOR_TYPE_REGULAR, DONOR_TYPE_USER } from 'utils/constants';
import messages from './messages';
import styles from './styles';

/* eslint-disable react/prefer-stateless-function */
class DonationsLiderboard extends React.Component {
  getLeaders = () => {
    const { donations } = this.props;
    const donorsData = donations.reduce((leaders, item) => {
      const result = { ...leaders };

      if (_isEmpty(result[item.email])) {
        result[item.email] = item;
      }

      result[item.email].amount += item.amount;

      return result;
    }, {});

    const donors = Object.values(donorsData);

    return donors.sort((a, b) => (a.amount > b.amount ? -1 : 1)).slice(0, 5);
  };

  renderIcon = (Icon, message) => (
    <Tooltip TransitionComponent={Zoom} title={<FormattedMessage {...message} />}>
      <ListItemIcon>
        <Icon className={this.props.classes.icon} />
      </ListItemIcon>
    </Tooltip>
  );

  render() {
    const { donations, classes } = this.props;
    const leaders = this.getLeaders();

    return (
      <Paper className={classes.root}>
        <div className={classes.boardWrapper}>
          {_isEmpty(donations) && (
            <H3>
              <br />No data
            </H3>
          )}
          <List>
            {leaders.map(
              (
                {
                  email = '',
                  amount = 0,
                  donation_type: donationType = DONATION_TYPE_ONE_TIME,
                  donor_type: donorType = DONOR_TYPE_USER,
                  created_at,
                },
                i,
              ) => (
                <Fragment key={`${email}${i + 1}`}>
                  <ListItem dense button>
                    <Avatar>
                      <ImageIcon />
                    </Avatar>
                    <ListItemText
                      primary={email.split('@')[0].toUpperCase()}
                      secondary={email}
                      className={classes.EmailText}
                    />
                    <ListItemText secondary={created_at} className={classes.EmailText} />
                    {donorType === DONOR_TYPE_REGULAR
                      ? this.renderIcon(StarIcon, messages.regular)
                      : this.renderIcon(StarBorderIcon, messages.user)}
                    <Divider />
                    {donationType === DONATION_TYPE_RECURRING
                      ? this.renderIcon(Filter9PlusIcon, messages.recurring)
                      : this.renderIcon(PlusOneIcon, messages.one_time)}
                    <ListItemText className={classes.DonationTypeText} />
                    <ListItemSecondaryAction>
                      <ListItemText secondary={`${Math.floor(amount || 0)}$`} />
                    </ListItemSecondaryAction>
                  </ListItem>
                  {i + 1 < leaders.length && <Divider />}
                </Fragment>
              ),
            )}
          </List>
        </div>
        <Comment />
      </Paper>
    );
  }
}

DonationsLiderboard.propTypes = {
  classes: PropTypes.object.isRequired,
  donations: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

export default withStyles(styles)(DonationsLiderboard);
