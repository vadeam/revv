export default theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  icon: {
    fontSize: 20,
    marginRight: 30,
  },
  table: {
    minWidth: 200,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  idCell: {
    display: 'absolute',
    width: '10px',
  },
});
