// import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Filter9PlusIcon from '@material-ui/icons/Filter9Plus';
import PlusOneIcon from '@material-ui/icons/PlusOne';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Zoom from '@material-ui/core/Zoom/index';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Tooltip from '@material-ui/core/Tooltip';

import { DONATION_TYPE_RECURRING, DONOR_TYPE_REGULAR } from 'utils/constants';
import Comment from 'components/Comment';
import H3 from 'components/H3';
import messages from 'components/DonationsLiderboard/messages';
import EnhancedTableHead from './EnhancedTableHead';
import styles from './styles';
import _isEmpty from 'lodash/isEmpty';

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1) : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

class EnhancedTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: 'asc',
      orderBy: 'amount',
      selected: [],
      page: 0,
      rowsPerPage: 5,
    };
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  renderIcon = (Icon, message) => (
    <Tooltip TransitionComponent={Zoom} title={<FormattedMessage {...message} />}>
      <ListItemIcon>
        <Icon className={this.props.classes.icon} />
      </ListItemIcon>
    </Tooltip>
  );

  render() {
    const { classes, donations = [] } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;

    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          {_isEmpty(donations) ? (
            <H3>
              <br />No data
            </H3>
          ) : (
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onRequestSort={this.handleRequestSort}
                rowCount={donations.length || 0}
              />
              <TableBody>
                {donations &&
                  donations
                    .sort(getSorting(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(n => (
                      <TableRow hover tabIndex={-1} key={n.id}>
                        <TableCell>{n.id}</TableCell>
                        <TableCell>{n.email}</TableCell>
                        <TableCell>{n.amount}$</TableCell>
                        <TableCell>
                          {n.donation_type === DONATION_TYPE_RECURRING
                            ? this.renderIcon(Filter9PlusIcon, messages.recurring)
                            : this.renderIcon(PlusOneIcon, messages.one_time)}
                        </TableCell>
                        <TableCell>
                          {n.donor_type === DONOR_TYPE_REGULAR
                            ? this.renderIcon(StarIcon, messages.regular)
                            : this.renderIcon(StarBorderIcon, messages.user)}
                        </TableCell>
                        <TableCell>{n.created_at}</TableCell>
                      </TableRow>
                    ))}
              </TableBody>
            </Table>
          )}
        </div>

        <TablePagination
          component="div"
          count={donations.length || 0}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{ 'aria-label': 'Previous Page' }}
          nextIconButtonProps={{ 'aria-label': 'Next Page' }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
        <Comment />
        <br />
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  donations: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

export default withStyles(styles)(EnhancedTable);
