/*
 * DonationsList Messages
 *
 * This contains all the text for the DonationsList component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  table: {
    id: 'app.components.DonationsList.table',
    defaultMessage: 'This is the EnhancedTable component !',
  },
  donation_type: {
    id: 'app.components.DonationsList.donation_type',
    defaultMessage: 'Donation type',
  },
  donor_type: {
    id: 'app.components.DonationsList.donor_type',
    defaultMessage: 'Donor type',
  },
});
