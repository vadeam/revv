import React from 'react';
import PropTypes from 'prop-types';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

const LeftDrawer = props => (
  <div tabIndex={0} role="button" onClick={props.toggleDrawer} onKeyDown={props.toggleDrawer}>
    <SwipeableDrawer open={props.open} onClose={() => null} onOpen={() => null} width={300}>
      <div>{props.menuItems}</div>
    </SwipeableDrawer>
  </div>
);

LeftDrawer.defaultProps = {
  menuItems: [],
};
LeftDrawer.propTypes = {
  toggleDrawer: PropTypes.func.isRequired,
  menuItems: PropTypes.array,
  open: PropTypes.bool,
};

export default LeftDrawer;
