/*
 * Comment Messages
 *
 * This contains all the text for the DonationsLiderboard component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  recurring: {
    id: 'app.components.comment.recurring',
    defaultMessage: 'Recurring donation',
  },
  one_time: {
    id: 'app.components.comment.one_time',
    defaultMessage: 'One-time donation',
  },
  regular: {
    id: 'app.components.comment.regular',
    defaultMessage: 'Regular donor',
  },
  user: {
    id: 'app.components.comment.regular',
    defaultMessage: 'User',
  },
});
