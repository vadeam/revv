import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles/index';
import PlusOneIcon from '@material-ui/icons/PlusOne';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Filter9PlusIcon from '@material-ui/icons/Filter9Plus';

import messages from './messages';
import styles from './styles';

const Comment = props => (
  <div className={props.classes.ChipWrapper}>
    <div className={props.classes.comment}>
      <StarIcon className={props.classes.iconComment} /> - <FormattedMessage {...messages.regular} />
    </div>
    <div className={props.classes.comment}>
      <StarBorderIcon className={props.classes.iconComment} /> - <FormattedMessage {...messages.user} />
    </div>
    <div className={props.classes.comment}>
      <Filter9PlusIcon className={props.classes.iconComment} /> - <FormattedMessage {...messages.recurring} />
    </div>
    <div className={props.classes.comment}>
      <PlusOneIcon className={props.classes.iconComment} /> - <FormattedMessage {...messages.one_time} />
    </div>
  </div>
);

Comment.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(Comment);
