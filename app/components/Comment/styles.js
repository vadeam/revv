export default () => ({
  iconComment: {
    fontSize: 20,
  },
  ChipWrapper: {
    display: 'flex',
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  comment: {
    marginRight: 20,
  },
});
