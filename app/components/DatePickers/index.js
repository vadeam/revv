import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import styles from './styles';

function DatePickers(props) {
  const { classes, id, label, type, value, defaultValue, onChange } = props;

  return (
    <form className={classes.container} noValidate>
      <TextField
        id={id}
        label={label}
        type={type}
        value={value}
        defaultValue={defaultValue}
        className={classes.textField}
        InputLabelProps={{ shrink: true }}
        onChange={onChange}
      />
    </form>
  );
}

DatePickers.defaultProps = {
  id: 'date',
  type: 'date',
  defaultValue: '',
};
DatePickers.propTypes = {
  classes: PropTypes.object.isRequired,
  id: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
};

export default withStyles(styles)(DatePickers);
