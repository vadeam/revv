import { Link } from 'react-router-dom';
import styled from 'styled-components';

export default styled(Link)`
  display: flex;
  padding: 0.25em 2em;
  margin: 1em;
  text-decoration: none;
  border-radius: 4px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: bold;
  font-size: 16px;
  border: 2px solid #41addd;
  color: #3f51b5;
  background: ${props => (props.active ? '#3f51b51c' : '#fff')};
  width: 200px;

  &:hover {
    background: #3f51b5;
    color: #fff;
  }

  & span {
    text-align: center;
    width: 100%;
  }
`;
