import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import LeftDrawer from 'components/LeftDrawer';
import HeaderLink from './HeaderLink';
import messages from './messages';
import styles from './styles';

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = { open: false };
  }

  toggleDrawer = () => this.setState(state => ({ open: !state.open }));

  getTitle = () => {
    const {
      location: { pathname },
    } = this.props;

    return pathname
      .split('/')
      .slice(1)
      .join()
      .toUpperCase();
  };

  render() {
    const {
      classes,
      location: { pathname },
    } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon onClick={this.toggleDrawer} />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              {this.getTitle() || 'DONATIONS'}
            </Typography>
          </Toolbar>

          <LeftDrawer
            open={this.state.open}
            toggleDrawer={this.toggleDrawer}
            menuItems={[
              <HeaderLink to="/" key="a1" active={pathname === '/' ? 'active' : null}>
                <FormattedMessage {...messages.donations} />
              </HeaderLink>,
              <HeaderLink to="/features" key="a2" active={pathname === '/features' ? 'active' : null}>
                <FormattedMessage {...messages.features} />
              </HeaderLink>,
              <HeaderLink to="/about" key="a3" active={pathname === '/about' ? 'active' : null}>
                <FormattedMessage {...messages.about} />
              </HeaderLink>,
            ]}
          />
        </AppBar>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default compose(
  withRouter,
  withStyles(styles),
)(Header);
