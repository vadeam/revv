export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const DONATION_TYPE_RECURRING = 'recurring';
export const DONATION_TYPE_ONE_TIME = 'one-time';

export const DONOR_TYPE_REGULAR = 'regular';
export const DONOR_TYPE_USER = 'user';
