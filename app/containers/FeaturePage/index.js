/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import H2 from 'components/H2';
import messages from './messages';
import List from './List';
import ListItem from './ListItem';
import ListItemTitle from './ListItemTitle';

export default class FeaturePage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Feature Page</title>
          <meta name="description" content="Feature page of React.js Boilerplate application" />
        </Helmet>
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <List>
          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.scaffoldingHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.scaffoldingMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.feedbackHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.feedbackMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.routingHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.routingMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.networkHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.networkMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.intlHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.intlMessage} />
            </p>
          </ListItem>

          <H2>
            <FormattedMessage {...messages.TechStack} />
          </H2>

          <List>
            <ListItem>
              <ListItemTitle>
                <FormattedMessage {...messages.Core} />
              </ListItemTitle>
              <ol>
                <li>React</li>
                <li>React Router</li>
                <li>Redux</li>
                <li>Redux Saga</li>
                <li>Reselect</li>
                <li>ImmutableJS</li>
                <li>Styled Components</li>
              </ol>
            </ListItem>
            <ListItem>
              <ListItemTitle>
                <FormattedMessage {...messages.UnitTesting} />
              </ListItemTitle>
              <ol>
                <li>Jest</li>
                <li>Enzyme</li>
              </ol>
            </ListItem>
            <ListItem>
              <ListItemTitle>
                <FormattedMessage {...messages.Linting} />
              </ListItemTitle>
              <ol>
                <li>ESLint</li>
                <li>Prettier</li>
                <li>stylelint</li>
              </ol>
            </ListItem>
          </List>
        </List>
      </div>
    );
  }
}
