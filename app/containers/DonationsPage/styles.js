import styled from 'styled-components';

export default theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
  },
  PickersWrapper: {
    marginTop: 30,
    marginBottom: 0,
    display: 'flex',
  },
  Picker: {
    marginLeft: 50,
    width: 150,
    height: 50,
    // border: '1px solid black',
  },
  DatePicker: {
    width: 150,
    padding: 4,
    marginTop: 1,
    background: '#fff',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.1)',
    border: '1px solid #f9f9f9',
  },
});

export const Wrapper = styled.div`
  margin-top: 20px;
`;
