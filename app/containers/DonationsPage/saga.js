/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';

import { LOAD_DONATIONS_REQUEST } from './constants';
import { donnationsLoadingError, donationsLoaded } from './actions';

/**
 *  Donations request/response handler
 */
export function* getDonations() {
  const requestURL = 'https://jyy74t2sj0.execute-api.us-east-1.amazonaws.com/v1/donations';

  try {
    const donationsList = yield call(request, requestURL);

    yield put(donationsLoaded(donationsList));
  } catch (err) {
    yield put(donnationsLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* donationsData() {
  yield takeLatest(LOAD_DONATIONS_REQUEST, getDonations);
}
