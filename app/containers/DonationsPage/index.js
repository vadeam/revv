/*
 * DonationsPage
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SwipeableViews from 'react-swipeable-views';
import { fromJS, is } from 'immutable';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import H2 from 'components/H2';
import H3 from 'components/H3';
import DonationChart from 'components/DonationChart';
import EnhancedTable from 'components/EnhancedTable';
import DonationsLiderboard from 'components/DonationsLiderboard';

import messages from './messages';
import Section from './Section';
import CenteredSection from './CenteredSection';
import { loadDonationsData } from './actions';
import reducer from './reducer';
import saga from './saga';
import { makeSelectDonations, makeSelectLoading, makeSelectError } from './selectors';
import styles, { Wrapper } from './styles';

/* eslint-disable react/prefer-stateless-function */
export class DonationsPage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      dateFrom: '',
      dateTo: '',
      picker: { from: null, to: null },
      donations: [],
    };
  }

  componentDidMount() {
    this.props.loadDonationsData();
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  handleDatePickersFrom = moment => {
    const { picker } = this.state;
    const dateFrom = moment ? moment.format() : '';

    picker.from = moment;
    this.setState(() => ({ picker, dateFrom }));
  };

  handleDatePickersTo = moment => {
    const { picker } = this.state;
    const dateTo = moment ? moment.format() : '';

    picker.to = moment;
    this.setState({ picker, dateTo });
  };

  render() {
    const { theme, classes } = this.props;
    const { donations, value, picker } = this.state;

    return (
      <article>
        <Helmet>
          <title>Donations Page</title>
          <meta name="description" content="A React.js Boilerplate application DonationsPage" />
        </Helmet>
        <Wrapper>
          <Section>
            <Tabs value={value} onChange={this.handleChange} indicatorColor="primary" textColor="primary" fullWidth>
              <Tab label={<FormattedMessage {...messages.chart} />} />
              <Tab label={<FormattedMessage {...messages.leaderboard} />} />
              <Tab label={<FormattedMessage {...messages.list} />} />
            </Tabs>

            <SwipeableViews
              axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
              index={value}
              onChangeIndex={this.handleChangeIndex}
            >
              <Typography component="div" dir={theme.direction} style={{ padding: 8 * 3 }}>
                <H2>
                  <FormattedMessage {...messages.chartDonations} />
                </H2>
                <CenteredSection>
                  <DonationChart donations={donations} />
                </CenteredSection>
              </Typography>

              <Typography component="div" dir={theme.direction} style={{ padding: 8 * 3 }}>
                <H2>
                  <FormattedMessage {...messages.leaderboardList} />
                </H2>
                <CenteredSection>
                  <DonationsLiderboard donations={donations} />
                </CenteredSection>
              </Typography>

              <Typography component="div" dir={theme.direction} style={{ padding: 8 * 3 }}>
                <H2>
                  <FormattedMessage {...messages.listDonations} />
                </H2>
                <CenteredSection>
                  <EnhancedTable donations={donations} />
                </CenteredSection>
              </Typography>
            </SwipeableViews>

            <div className={classes.PickersWrapper}>
              <H3>
                <FormattedMessage {...messages.filterByDate} />:
              </H3>

              <div className={classes.Picker}>
                Date from
                <DatePicker
                  showYearDropdown
                  fixedHeight
                  selected={picker.from}
                  className={classes.DatePicker}
                  isClearable={true}
                  todayButton={'Today'}
                  onChange={this.handleDatePickersFrom}
                />
              </div>
              <div className={classes.Picker}>
                Date to
                <DatePicker
                  showYearDropdown
                  fixedHeight
                  selected={picker.to}
                  className={classes.DatePicker}
                  isClearable={true}
                  todayButton={'Today'}
                  onChange={this.handleDatePickersTo}
                />
              </div>
            </div>
          </Section>
        </Wrapper>
      </article>
    );
  }
}

DonationsPage.getDerivedStateFromProps = (props, state) => {
  const { donations: propsDonations } = props;
  const { donations: stateDonations } = state;

  if (is(fromJS(stateDonations), fromJS(propsDonations))) return null;

  const dateFrom = Date.parse(state.dateFrom) || false;
  const dateTo = Date.parse(state.dateTo) || false;
  const donations = propsDonations
    .map(d => {
      const createdAt = d.created_at * 1000;
      let flag;

      !dateFrom && !dateTo && (flag = true);
      dateFrom && dateTo && (flag = createdAt >= dateFrom && createdAt <= dateTo);
      dateFrom && !dateTo && (flag = createdAt >= dateFrom);
      !dateFrom && dateTo && (flag = createdAt <= dateTo);

      if (!flag) return null;

      return {
        ...d,
        created_at: new Date(d.created_at * 1000).toLocaleDateString(),
        amount: parseFloat(d.amount / 100),
      };
    })
    .filter(d => d);

  return { donations };
};

DonationsPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  donations: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  loadDonationsData: PropTypes.func,
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export function mapDispatchToProps(dispatch) {
  return {
    loadDonationsData: () => dispatch(loadDonationsData()),
  };
}

const mapStateToProps = createStructuredSelector({
  isFetching: makeSelectLoading(),
  error: makeSelectError(),
  donations: makeSelectDonations(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'donations', reducer });
const withSaga = injectSaga({ key: 'donations', saga });
const withStyle = withStyles(styles, { withTheme: true });

export default compose(
  withStyle,
  withReducer,
  withSaga,
  withConnect,
)(DonationsPage);
