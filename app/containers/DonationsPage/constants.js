/*
 * DonationsConstants
 */

export const LOAD_DONATIONS_REQUEST = 'boilerplate/Donations/LOAD_DONATIONS_REQUEST';
export const LOAD_DONATIONS_SUCCESS = 'boilerplate/Donations/LOAD_DONATIONS_SUCCESS';
export const LOAD_DONATIONS_FAILURE = 'boilerplate/Donations/LOAD_DONATIONS_FAILURE';
