/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  donationsSection: {
    id: 'boilerplate.containers.DonationsPage.header',
    defaultMessage: 'Donations',
  },
  chart: {
    id: 'boilerplate.containers.DonationsPage.chart',
    defaultMessage: 'Chart',
  },
  chartDonations: {
    id: 'boilerplate.containers.DonationsPage.chartDonations',
    defaultMessage: 'Donations chart',
  },
  list: {
    id: 'boilerplate.containers.DonationsPage.list',
    defaultMessage: 'List',
  },
  listDonations: {
    id: 'boilerplate.containers.DonationsPage.listDonations',
    defaultMessage: 'List of donations',
  },
  leaderboard: {
    id: 'boilerplate.containers.DonationsPage.leaderboard',
    defaultMessage: 'Leaderboard',
  },
  leaderboardList: {
    id: 'boilerplate.containers.DonationsPage.leaderboardList',
    defaultMessage: 'Leader board list',
  },
  filterByDate: {
    id: 'boilerplate.containers.DonationsPage.filterByDate',
    defaultMessage: 'Filter by date',
  },
});
