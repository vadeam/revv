/**
 * DonationPage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectDonations = state => state.get('donations', initialState);

const makeSelectLoading = () => createSelector(selectDonations, donationsState => donationsState.get('isFetching'));

const makeSelectError = () => createSelector(selectDonations, donationsState => donationsState.get('error'));

const makeSelectDonations = () => createSelector(selectDonations, donationsState => donationsState.get('list'));

export { selectDonations, makeSelectDonations, makeSelectLoading, makeSelectError };
