/*
 * DonationReducer
 */
import { fromJS } from 'immutable';

import { LOAD_DONATIONS_REQUEST, LOAD_DONATIONS_SUCCESS, LOAD_DONATIONS_FAILURE } from './constants';

export const initialState = fromJS({
  isFetching: false,
  list: [],
  error: false,
});

function donationsReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_DONATIONS_REQUEST:
      return state
        .set('isFetching', true)
        .set('list', [])
        .set('error', false);
    case LOAD_DONATIONS_SUCCESS:
      const donations = Array.isArray(action.payload)
        ? action.payload.slice(0, 30).sort((a, b) => (a.created_at > b.created_at ? 1 : -1))
        : [];

      return state.set('isFetching', false).set('list', donations);
    case LOAD_DONATIONS_FAILURE:
      return state.set('error', action.error).set('isFetching', false);
    default:
      return state;
  }
}

export default donationsReducer;
