/*
 * Donations Actions
 */

import { LOAD_DONATIONS_REQUEST, LOAD_DONATIONS_SUCCESS, LOAD_DONATIONS_FAILURE } from './constants';

/**
 * Loads donnations
 *
 * @return {object} An action object with a type of LOAD_DONATIONS_REQUEST
 */
export function loadDonationsData() {
  return {
    type: LOAD_DONATIONS_REQUEST,
  };
}

/**
 * Dispatched when the donations are loaded by the request saga
 *
 * @param  {array} donations The donations data
 *
 * @return {object} An action object with a type of LOAD_DONATIONS_SUCCESS passing the donations
 */
export function donationsLoaded(donations) {
  return {
    type: LOAD_DONATIONS_SUCCESS,
    payload: donations,
  };
}

/**
 * Dispatched when loading the donations fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of LOAD_DONATIONS_FAILURE passing the error
 */
export function donnationsLoadingError(error) {
  return {
    type: LOAD_DONATIONS_FAILURE,
    error,
  };
}
