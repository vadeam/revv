/*
 * AboutPage Messages
 *
 * This contains all the text for the AboutPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  about: {
    id: 'boilerplate.containers.AboutPage.start_project.about',
    defaultMessage: 'About',
  },
  startProjectHeader: {
    id: 'boilerplate.containers.AboutPage.start_project.header',
    defaultMessage: 'Quick setup for new performance orientated, offline–first React.js applications.',
  },
  startProjectMessage: {
    id: 'boilerplate.containers.AboutPage.start_project.message',
    defaultMessage:
      'A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices',
  },
  trymeHeader: {
    id: 'boilerplate.containers.AboutPage.tryme.header',
    defaultMessage: 'Try me!',
  },
  trymeMessage: {
    id: 'boilerplate.containers.AboutPage.tryme.message',
    defaultMessage: 'Show Github repositories by',
  },
  trymeAtPrefix: {
    id: 'boilerplate.containers.AboutPage.tryme.atPrefix',
    defaultMessage: '@',
  },
});
